(function ($) {
  'use strict';

  /**
   * Slideshows
   */

  var sliders = document.querySelectorAll( '.js-carousel' );
  var slider_init;

  if ( sliders.length ) {
    sliders.forEach(function (slider) {
      slider_init = tns({
        container: slider,
        mouseDrag: false,
        speed: 800,
        autoplay: true,
        autoHeight: true,
        autoplayHoverPause: true,
      });
    });
  }


  /**
   * Apply Now Select
   */

  $('#apply_now_select').on('change', function () {
    var url = $(this).val();

    if ( url ) {
      window.location = url;
    }

    return false;
  });


  /**
   * Team Locations Map
   */

  var $svg_map_button   = $('.map-location-button');
  var $map_sidebar_link = $('.navigation--sidebar a');

  $svg_map_button.on('click', function () {
    var $this = $(this);
    var this_category = $this.data( 'category' );
    var $this_nav_link = $('.navigation--sidebar').find( 'a[data-category="' + this_category + '"]' );

    $this_nav_link[0].click();
  });

  $svg_map_button.on('mouseover', function () {
    var $this = $(this);
    var this_category = $this.data( 'category' );
    var $this_nav_link = $('.navigation--sidebar').find( 'a[data-category="' + this_category + '"]' );

    if ( ! $this_nav_link.hasClass('is-active') ) {
      $this_nav_link.addClass( 'is-active' );
    }
  });

  $svg_map_button.on('mouseout', function () {
    var $this = $(this);
    var this_category = $this.data( 'category' );
    var $this_nav_link = $('.navigation--sidebar').find( 'a[data-category="' + this_category + '"]' );

    if ( $this_nav_link.hasClass('is-active') ) {
      $this_nav_link.removeClass( 'is-active' );
    }
  });

  $map_sidebar_link.on('mouseover', function () {
    var $this = $(this);
    var this_category = $this.data( 'category' );
    var $this_map_button = $('.team-map-svg').find( 'path[data-category="' + this_category + '"]' );

    if ( ! $this_map_button.hasClass('is-active') ) {
      $this_map_button.addClass( 'is-active' );
    }
  });

  $map_sidebar_link.on('mouseout', function () {
    var $this = $(this);
    var this_category = $this.data( 'category' );
    var $this_map_button = $('.team-map-svg').find( 'path[data-category="' + this_category + '"]' );

    if ( $this_map_button.hasClass('is-active') ) {
      $this_map_button.removeClass( 'is-active' );
    }
  });

})(jQuery);
