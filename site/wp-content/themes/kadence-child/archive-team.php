<?php
/**
 * The main archive template file
 *
 * @package kadence-child
 */

namespace Kadence;

get_header();

kadence()->print_styles( 'kadence-content' );
/**
 * Hook for main archive content.
 */
do_action( 'kadence_archive' );

get_footer();
