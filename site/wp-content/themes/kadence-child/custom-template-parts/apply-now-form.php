<?php
/**
 * Displays the Apply Now Form
 */
?>
<?php if ( have_rows( 'apply_now_links', 'option' ) ) : ?>
  <div class="apply-now-form">
    <label for="apply_now_select"><b><?php _e( 'Apply Now', 'hma_mortgage' ); ?></b></label>
    <select id="apply_now_select" name="apply_now_select" class="apply-now-select">
      <option value="" selected><?php _e( 'Choose Loan Officer', 'hma_mortgage' ); ?></option>
      <?php while ( have_rows( 'apply_now_links', 'option' )) : the_row(); ?>
        <?php
          $name     = get_sub_field( 'name' );
          $link_url = get_sub_field( 'link_url' );
        ?>
        <?php if ( ! empty( $name ) && ! empty( $link_url )) : ?>
          <option value="<?php echo esc_url( $link_url ); ?>"><?php echo $name; ?></option>
        <?php endif; ?>
      <?php endwhile; ?>
    </select>
  </div>
<?php endif; ?>
