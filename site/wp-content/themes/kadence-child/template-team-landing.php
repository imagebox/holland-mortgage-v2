<?php
/**
 * Template Name: Team Landing Page
 * 
 * The main single item template file.
 * 
 * @package kadence-child
 */

namespace Kadence;

get_header();

kadence()->print_styles( 'kadence-content' );
/**
 * Hook for everything, makes for better elementor theming support.
 */
// do_action( 'kadence_single' );

// Team Landing Page
get_template_part( 'template-parts/content/custom-team-landing' );

get_footer();
