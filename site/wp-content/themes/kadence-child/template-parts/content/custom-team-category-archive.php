<?php
/**
 * Custom Team Archive
 *
 * @package kadence-child
 */

$team_cat_ID = '';
$queried_object = get_queried_object();

if ( $queried_object && isset($queried_object->term_id) ) {
  $team_cat_ID = $queried_object->term_id;
}

?>
<section class="">

  <div class="l-sidebar l-sidebar--align-top l-sidebar--left">
    <div class="l-aside-col">

      <?php
        $team_category_terms = get_terms(array(
          'taxonomy'    => 'team_category',
          'hide_empty'  => false,
          'parent'      => 0,
          'exclude'     => array( 9 ),
        ));
      ?>
      <?php if ( $team_category_terms && ! is_wp_error($team_category_terms) ) : ?>
        <nav class="navigation--sidebar" role="navigation"
          aria-label="<?php _e( 'Team Locations', 'kadence-child' ); ?>">
          <ul>
            <?php foreach ( $team_category_terms as $term ) : ?>
              <li>
                <a class="<?php
                    if ( $term->term_id == $team_cat_ID ) {
                      echo 'is-active';
                    }
                  ?>"
                  href="<?php echo get_term_link( $term ); ?>"
                  data-category="<?php echo $term->slug; ?>">
                  <?php echo $term->name; ?>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>
        </nav>
      <?php endif; ?>
    </div>
    <div class="l-main-col">

      <?php if ( have_posts() ) : ?>
        <section class="category-section category-section--parent">
          <div class="l-grid l-grid--4-col">

            <?php while ( have_posts() ) : the_post(); ?>
              <?php
                $photo = get_field( 'photo' );
                $title = get_field( 'title' );
                $team_category_terms = get_the_terms( get_the_ID(), 'team_category' );
              ?>

              <div class="l-grid-item">
                <a class="team-card" href="<?php echo get_the_permalink(); ?>">
                  <?php if ( $photo ) : ?>
                    <div class="team-card-photo">
                      <img src="<?php echo $photo['url']; ?>"
                        width="<?php echo $photo['width']; ?>"
                        height="<?php echo $photo['height']; ?>"
                        alt="<?php echo get_the_title(); ?> <?php _e( 'employee headshot', 'kadence-child' ); ?>">
                    </div>
                  <?php endif; ?>

                  <h6 class="team-card-title"><?php echo get_the_title(); ?></h6>

                  <?php if ( ! empty( $title )) : ?>
                    <p class="team-card-job-title"><?php echo $title; ?></p>
                  <?php endif; ?>

                  <?php if ( $team_category_terms
                    && ! is_wp_error( $team_category_terms )
                    && ! is_tax( 'team_category', 'leadership' )
                    && ! is_tax( 'team_category', 'dave-holland-team' ) ) : ?>
                    <?php
                      $us_state_abbrs = array();

                      // Get term names as abbreviated states
                      foreach ( $team_category_terms as $term ) {
                        $term_name_abbr = state_abbr( $term->name );

                        if ( ! empty($term_name_abbr) ) {
                          $us_state_abbrs[] = $term_name_abbr;
                        }
                      }
                    ?>
                    <?php if ( $us_state_abbrs ) : ?>
                      <div class="team-card-footer">
                        <p class="team-card-cats"><?php echo implode( ', ', $us_state_abbrs ); ?></p>
                      </div>
                    <?php endif; ?>
                  <?php endif; ?>
                </a>
              </div>

            <?php endwhile; ?>

          </div>
        </section>

      <?php endif; ?>

      <?php // Child Category Query ?>
      <?php
        $team_category_child_term_ids = get_term_children( $team_cat_ID, 'team_category' );
      ?>
      <?php if ( $team_category_child_term_ids ) : ?>
        <?php foreach ( $team_category_child_term_ids as $child_term_id ) : ?>
          <?php
            $child_term = get_term( $child_term_id, 'team_category' );

            // Query for team members in child category
            $query_child_term_team = new WP_Query(array(
              'post_type' => 'team',
              'posts_per_page' => -1,
              'tax_query' => array(
                array(
                  'taxonomy' => 'team_category',
                  'terms' => $child_term->term_id,
                ),
              ),
            ));
          ?>
          <?php if ( $query_child_term_team->have_posts() ) : ?>
            <section class="category-section category-section--child">
              <header class="category-section-header">
                <h2><?php echo $child_term->name; ?></h2>
              </header>
              <div class="l-grid l-grid--4-col">
                <?php while ( $query_child_term_team->have_posts() ) : $query_child_term_team->the_post(); ?>
                  <?php
                    $photo = get_field( 'photo' );
                    $title = get_field( 'title' );
                    $team_category_terms = get_the_terms( get_the_ID(), 'team_category' );
                  ?>

                  <div class="l-grid-item">
                    <a class="team-card" href="<?php echo get_the_permalink(); ?>">
                      <?php if ( $photo ) : ?>
                        <div class="team-card-photo">
                          <img src="<?php echo $photo['url']; ?>"
                            width="<?php echo $photo['width']; ?>"
                            height="<?php echo $photo['height']; ?>"
                            alt="<?php echo get_the_title(); ?> <?php _e( 'employee headshot', 'kadence-child' ); ?>">
                        </div>
                      <?php endif; ?>

                      <h6 class="team-card-title"><?php echo get_the_title(); ?></h6>

                      <?php if ( ! empty( $title )) : ?>
                        <p class="team-card-job-title"><?php echo $title; ?></p>
                      <?php endif; ?>

                      <?php if ( $team_category_terms
                        && ! is_wp_error( $team_category_terms )
                        && ! is_tax( 'team_category', 'leadership' )
                        && ! is_tax( 'team_category', 'dave-holland-team' ) ) : ?>
                        <?php
                          $us_state_abbrs = array();

                          // Get term names as abbreviated states
                          foreach ( $team_category_terms as $term ) {
                            $term_name_abbr = state_abbr( $term->name );

                            if ( ! empty($term_name_abbr) ) {
                              $us_state_abbrs[] = $term_name_abbr;
                            }
                          }
                        ?>
                        <?php if ( $us_state_abbrs ) : ?>
                          <div class="team-card-footer">
                            <p class="team-card-cats"><?php echo implode( ', ', $us_state_abbrs ); ?></p>
                          </div>
                        <?php endif; ?>
                      <?php endif; ?>
                    </a>
                  </div>
                <?php endwhile; ?>
              </div>
            </section>
            <?php wp_reset_postdata(); ?>
          <?php endif; ?>
        <?php endforeach; ?>
      <?php endif; ?>

    </div>
  </div>

</section>
