<?php
/**
 * Template part for displaying a post or page.
 *
 * @package kadence-child
 */

namespace Kadence;

?>
<?php
if ( kadence()->show_feature_above() ) {
	get_template_part( 'template-parts/content/entry_thumbnail', get_post_type() );
}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry content-bg single-entry' ); ?>>
	<div class="entry-content-wrap">
	<?php
		do_action( 'kadence_single_before_inner_content' );

		// if ( kadence()->show_in_content_title() ) {
		// get_template_part( 'template-parts/content/entry_header', get_post_type() );
		// }
		// if ( kadence()->show_feature_below() ) {
		// get_template_part( 'template-parts/content/entry_thumbnail', get_post_type() );
		// }
	?>

	<?php
		$photo         = get_field( 'photo' );
		$title         = get_field( 'title' );
		$phone_title 	 = get_field( 'phone_title' );
		$phone         = get_field( 'phone' );
		$email         = get_field( 'email' );
		$nmls          = get_field( 'nmls#' );
		$description   = get_field( 'description' );
		$meetteam      = get_field( 'meetteam' );
		$linkedin      = get_field( 'linkedin' );
		$zillow      	 = get_field( 'zillow' );
		$facebook_profile_url = get_field( 'facebook_profile_url' );
		$apply_now_url = get_field( 'apply_now_url' );
	?>

	<div class="l-sidebar">
	  <div class="l-aside-col">

		<?php if ( $photo ) : ?>
			<div class="team-sidebar-photo">
			<img src="<?php echo esc_url( $photo['url'] ); ?>"
				width="<?php echo $photo['width']; ?>"
				height="<?php echo $photo['height']; ?>"
				alt="<?php echo $photo['alt']; ?>">
			</div>
		<?php endif; ?>

		<div class="team-sidebar-content">
			<?php if ( ! empty( $phone ) ) : ?>
				<?php
					// Strip hyphens & parenthesis for tel link.
					$tel_formatted = str_replace( array( '.', '-', '–', '(', ')', ' ' ), '', $phone );
				?>
				<p>
					<b><?php
						if ( ! empty($phone_title) ) {
							echo $phone_title . ':';
						} else {
							_e( 'Phone:', 'boxpress' );
						}
					?></b>
					<a href="tel:+1<?php echo $tel_formatted; ?>"><?php echo $phone; ?></a>
				</p>
			<?php endif; ?>

			<?php if ( have_rows( 'additional_phone_numbers' ) ) : ?>
				<?php while ( have_rows( 'additional_phone_numbers' ) ) : the_row(); ?>
					<?php
						$phone_title 	= get_sub_field( 'phone_title' );
						$phone_number = get_sub_field( 'phone_number' );
					?>
					<?php if ( ! empty($phone_number) ) : ?>
						<?php
							// Strip hyphens & parenthesis for tel link.
							$tel_formatted = str_replace( array( '.', '-', '–', '(', ')', ' ' ), '', $phone_number );
						?>
						<p>
							<b><?php
								if ( ! empty($phone_title) ) {
									echo $phone_title . ':';
								} else {
									_e( 'Phone:', 'boxpress' );
								}
							?></b>
							<a href="tel:+1<?php echo $tel_formatted; ?>"><?php echo $phone_number; ?></a>
						</p>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php endif; ?>

			<?php if ( ! empty( $email ) ) : ?>
			<p class="sidebar-link--email"><b><?php _e( 'Email:', 'boxpress' ); ?></b> <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
			<?php endif; ?>

			<?php if ( ! empty( $nmls ) ) : ?>
			<p><b><?php _e( 'NMLS#:', 'boxpress' ); ?></b> <?php echo $nmls; ?></p>
			<?php endif; ?>

			<?php if ( ! empty( $meetteam ) || ! empty( $linkedin ) || ! empty($zillow) || ! empty($facebook_profile_url) || ! empty( $apply_now_url ) ) : ?>
			<div class="team-sidebar-buttons">
				<?php
				if ( $meetteam ) :
						$mt_button_url    = $meetteam['url'];
						$mt_button_title  = $meetteam['title'];
						$mt_button_target = $meetteam['target'] ? $meetteam['target'] : '_self';
					?>
					<a class="button" href="<?php echo esc_url( $mt_button_url ); ?>" target="<?php echo esc_attr( $mt_button_target ); ?>"><?php echo esc_html( $mt_button_title ); ?></a>
				<?php endif; ?>

				<?php if ( ! empty( $linkedin ) || ! empty($zillow) || ! empty($facebook_profile_url) || ! empty( $apply_now_url ) ) : ?>
					<ul>
					<?php if ( ! empty( $apply_now_url ) ) : ?>
						<li>
						<a class="button" href="<?php echo esc_url( $apply_now_url ); ?>" target="_blank" rel="nofollow noopener"><?php _e( 'Apply Now', 'boxpress' ); ?> <div class="button-arrow"><div class="arrow"></div></div></a>
						</li>
					<?php endif; ?>
					<?php if ( ! empty( $linkedin ) ) : ?>
						<li>
						<a class="social-button" href="<?php echo esc_url( $linkedin ); ?>" target="_blank" rel="nofollow noopener">
							<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/child-assets/img/social-icons/social-linkedin.png"
							width="42"
							height="42"
							alt="LinkedIn Profile">
						</a>
						</li>
					<?php endif; ?>
					<?php if ( ! empty( $facebook_profile_url ) ) : ?>
						<li>
						<a class="social-button" href="<?php echo esc_url( $facebook_profile_url ); ?>" target="_blank" rel="nofollow noopener">
							<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/child-assets/img/social-icons/social-facebook.png"
							width="42"
							height="42"
							alt="Facebook Profile">
						</a>
						</li>
					<?php endif; ?>
					<?php if ( ! empty($zillow) ) : ?>
						<li>
						<a class="social-button" href="<?php echo esc_url( $zillow ); ?>" target="_blank" rel="nofollow noopener">
							<img src="<?php bloginfo( 'stylesheet_directory' ); ?>/child-assets/img/social-icons/social-zillow.png"
							width="42"
							height="42"
							alt="Zillow Profile">
						</a>
						</li>
					<?php endif; ?>
					</ul>
				<?php endif; ?>

			</div>
			<?php endif; ?>
		</div>

	  </div>
	  <div class="l-main-col">
		<header>
		  <h1><?php the_title(); ?></h1>
		  <?php if ( ! empty( $title ) ) : ?>
			<p class="team-member-job-title"><?php echo $title; ?></p>
		  <?php endif; ?>
		</header>

		<?php if ( ! empty( $description ) ) : ?>
		  <div class="profile-description">
			<?php echo $description; ?>
		  </div>
		<?php endif; ?>

		<?php if ( have_rows( 'review_slides' ) ) : ?>
		  <section class="team-review-slideshow">
			<div class="js-carousel">
			  <?php
				while ( have_rows( 'review_slides' ) ) :
					the_row();
					?>
					<?php
					$review_stars = get_sub_field( 'review_stars' );
					$review_quote = get_sub_field( 'review_quote' );
					$review_name  = get_sub_field( 'review_name' );
					$review_url   = get_sub_field( 'review_url' );
					?>
					<?php if ( ! empty( $review_quote ) ) : ?>

				  <div class="review-slide">
					<blockquote class="review-quote">
						<?php if ( ! empty( $review_stars ) ) : ?>
						<div class="reviews-stars">
						  <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/child-assets/img/reviews/review-stars-<?php echo $review_stars; ?>.png" alt="<?php echo $review_stars; ?> Stars">
						</div>
					  <?php endif; ?>
					  <div class="review-quote-text">
						<p><?php echo $review_quote; ?></p>
					  </div>
					  <div class="review-quote-footer">
						<?php if ( ! empty( $review_name ) ) : ?>
						  <cite class="review-quote-citation">- <?php echo $review_name; ?></cite>
						<?php endif; ?>
						<?php if ( ! empty( $review_url ) ) : ?>
						  <a class="review-quote-link" href="<?php echo esc_url( $review_url ); ?>" target="_blank"><?php _e( 'Read the review on Google', 'boxpress' ); ?></a>
						<?php endif; ?>
					  </div>
					</blockquote>
				  </div>

				<?php endif; ?>
				<?php endwhile; ?>
			</div>
		  </section>
		<?php endif; ?>
	  </div>
	</div>

	<?php
	  get_template_part( 'template-parts/content/entry_content', get_post_type() );
	if ( 'post' === get_post_type() && kadence()->option( 'post_tags' ) ) {
		get_template_part( 'template-parts/content/entry_footer', get_post_type() );
	}

	  do_action( 'kadence_single_after_inner_content' );
	?>
  </div>
</article><!-- #post-<?php the_ID(); ?> -->

<?php
if ( is_singular( get_post_type() ) ) {
	if ( 'post' === get_post_type() && kadence()->option( 'post_author_box' ) ) {
		get_template_part( 'template-parts/content/entry_author', get_post_type() );
	}
	// Show post navigation only when the post type is 'post' or has an archive.
	if ( ( 'post' === get_post_type() || get_post_type_object( get_post_type() )->has_archive ) && kadence()->show_post_navigation() ) {
		the_post_navigation(
			apply_filters(
				'kadence_post_navigation_args',
				array(
					'prev_text' => '<div class="post-navigation-sub"><small>' . kadence()->get_icon( 'arrow-left-alt' ) . esc_html__( 'Previous', 'kadence' ) . '</small></div>%title',
					'next_text' => '<div class="post-navigation-sub"><small>' . esc_html__( 'Next', 'kadence' ) . kadence()->get_icon( 'arrow-right-alt' ) . '</small></div>%title',
				)
			)
		);
	}
	if ( 'post' === get_post_type() && kadence()->option( 'post_related' ) ) {
		get_template_part( 'template-parts/content/entry_related', get_post_type() );
	}
	// Show comments only when the post type supports it and when comments are open or at least one comment exists.
	if ( kadence()->show_comments() ) {
		comments_template();
	}
}
