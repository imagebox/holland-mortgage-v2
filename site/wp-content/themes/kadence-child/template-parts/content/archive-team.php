<?php
/**
 * The main archive template file for inner content.
 *
 * @package kadence-child
 */

namespace Kadence;

/**
 * Hook for Hero Section
 */
do_action( 'kadence_hero_header' );
?>
<div id="primary" class="content-area <?php
		if ( is_archive() ) {
			echo 'content-area--team-archive';
		} elseif ( is_tax() ) {
			echo 'content-area--team-category';
		}
	?>">
	<div class="content-container site-container">
		<main id="main" class="site-main" role="main">
			<?php
				/**
				 * Hook for anything before main content
				 */
				do_action( 'kadence_before_main_content' );

				if ( kadence()->show_in_content_title() ) {
					get_template_part( 'template-parts/content/archive_header' );
				}

				// Taxonomy Listing Template
				if ( is_tax() && have_posts() ) {
					get_template_part( 'template-parts/content/custom-team-category-archive' );

				// Archive - Map Landing Template
				} elseif ( is_archive() && have_posts() ) {
					get_template_part( 'template-parts/content/custom-team-archive' );

				// No content or template fallback
				} else {
					get_template_part( 'template-parts/content/error' );
				}

				/**
				 * Hook for anything after main content
				 */
				do_action( 'kadence_after_main_content' );
			?>
		</main><!-- #main -->
		
		<?php get_sidebar(); ?>
	</div>
</div><!-- #primary -->
