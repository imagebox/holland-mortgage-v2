<?php
/**
 * Document Uploads
 * ---
 * - Creates an upload folder for each attorney
 * - Renames each files to include attorney name
 */


/**
 * Change Doc Upload Path
 */

add_filter( 'gform_upload_path', 'custom_upload_path', 10, 2 );
function custom_upload_path( $path_info, $form_id ) {

  // Only apply to the Document Upload form
  if ( $form_id === 2 ) {

    $site_url   = get_site_url();
    $path_info  = array();

    // Example `WP_CONTENT_DIR` - '/mnt/BLOCKSTORAGE/home/490872.cloudwaysapps.com/aybjxqszsp/public_html/wp-content'
    // Example `WP_CONTENT_URL` - 'https://hollandmortgage.imagebox.dev/wp-content'
    // *Note* - No Trailing slash (/) in strings

    $path_info['path']  = WP_CONTENT_DIR . '/uploads/document_upload/';
    $path_info['url']   = WP_CONTENT_URL .  '/uploads/document_upload/';

    return $path_info;
  }
}



/**
 * this is a gf filter which lets us
 * get an entry after it is done.
 */

function endswith( $string, $test ) {
  $strlen   = strlen( $string );
  $testlen  = strlen( $test );
  if ( $testlen > $strlen ) return false;
  return substr_compare( $string, $test, $strlen - $testlen, $testlen ) === 0;
}



/**
 * Handle File Uploads
 * ---
 * - Rename files to prepend the Staff Member name
 * - Add the renamed files to the notification email
 */

add_filter( 'gform_notification_2', 'rw_notification_attachments', 10, 3 );
function rw_notification_attachments( $notification, $form, $entry ) {
  $attachment_file_urls = array();

  /**
   * Get the 'Staff Member' field value
   */

  $staff_field_ID     = 3;
  $staff_field        = GFFormsModel::get_field( $form, $staff_field_ID );
  $staff_field_value  = is_object( $staff_field ) ? $staff_field->get_value_export( $entry ) : '';

  // Convert name to lowercase and remove spaces
  $client_folder      = str_replace( ' ', '-', strtolower( $staff_field_value ));


  /**
   * Get all 'File Upload' fields from the form
   */

  foreach ( $form['fields'] as $key => $field ) {
    if ( $field['type'] == 'fileupload' ) {
      $keys[] = $field['id'];
    }
  }

  // Exit if no files exist
  if ( ! is_array( $keys ) ) {
    return;
  }


  /**
   * Loop over all uploaded files and create new names
   * with the selected Staff Member appended before
   */

  foreach ( $keys as $value ) {

    // Convert the entry data json into array
    $value_paths = json_decode( $entry[ $value ]);

    if ( ! is_array( $value_paths ) ) {
      return;
    }
    
    // Loop over all uploaded files in the upload field
    foreach ( $value_paths as $file_path => $value ) {

      // Get file upload info
      $pathinfo = pathinfo( $value );

      if ( ! empty( $pathinfo['extension'] )) {
        $oldurls[$file_path] = $pathinfo;

        // Rename file `team-member_file-name.extension`
        $pathinfo['filename'] = $client_folder . '_' . $pathinfo['filename'];
        $newurls[$file_path] = $pathinfo;
      }
    } 
  }

  if ( ! is_array( $newurls )) {
    return;
  }

  // Get custom upload path
  $uploadinfo   = custom_upload_path( '', 2 );

  print_r( $uploadinfo );

  // Create person folder
  $uploads_dir  = $uploadinfo['path'];
  $person_dir   = $uploads_dir . $client_folder . '/';

  if ( ! is_dir( $person_dir )) {
    mkdir( $person_dir, 0755 );
  }


  /**
   * Set new Paths and URLs
   */

  // Loop over our sets of file URLs
  foreach ( $newurls as $key => $value ) {

    // Old - URLs to replace
    $oldpath  = $uploadinfo['path'] . $oldurls[$key]['filename'] . '.' . $oldurls[$key]['extension'];
    $oldurl   = $uploadinfo['url']  . $oldurls[$key]['filename'] . '.' . $oldurls[$key]['extension'];

    // New - New URLs to replace with
    $newpath  = $uploadinfo['path'] . $client_folder . '/' . $newurls[$key]['filename'] . '.' . $newurls[$key]['extension'];
    $newurl   = $uploadinfo['url']  . $client_folder . '/' . $newurls[$key]['filename'] . '.' . $newurls[$key]['extension'];


    /**
     * If the new file path already exists,
     * append `$i` to the filename.
     */

    $i = 1;

    while ( file_exists( $newpath )) {
      $newpath  = $uploadinfo['path'] . $client_folder . '/' . $newurls[$key]['filename'] . '-' . $i . '.' . $newurls[$key]['extension'];
      $newurl   = $uploadinfo['url']  . $client_folder . '/' . $newurls[$key]['filename'] . '-' . $i . '.' . $newurls[$key]['extension'];
      $i++;
    }

    // Add new url to attachments array
    $attachment_file_urls[] = $newurl;


    /**
     * Rename the file and update the DB
     * with the new name.
     */

    // Rename the file
    $is_success = rename( $oldpath, $newpath );

    // For testing purposes, use copy instead of rename
    // $is_success = copy( $oldpath, $newpath );

    $lead_table = GFFormsModel::get_lead_details_table_name();

    if ( $is_success &&
         ! empty( $lead_table )) {

      global $wpdb;
      $wpdb->update(
        $lead_table,
        array( "value" => $newpath ),
        array( "lead_id" => $entry["id"], "value" => $oldpath )
      );
    }
  }



  /**
   * Generate Notification with files attached
   */

  // Use Test Notification
  if ( $notification['name'] == 'Admin Notification' ||
       $notification['name'] == 'Test Notification' ) {

    if ( $attachment_file_urls ) {

      $notification['message'] .= '<p><strong>Document File Upload</strong></p>';
      $notification['message'] .= '<ul>';

      foreach ( $attachment_file_urls as $file_url ) {
        $pathinfo       = pathinfo( $file_url );
        $file_name      = $pathinfo['filename'];
        $file_extension = $pathinfo['extension'];

        $new_file_name  = $file_name . '.' . $file_extension;
        $new_file_url   = doc_upload_format_url( $new_file_name, $client_folder );

        // $notification['message'] .= '<li><a href="' . $file_url . '">' . $file_name . '</a></li>';
        $notification['message'] .= '<li><a href="' . $new_file_url . '">' . $file_name . '</a></li>';
      }

      $notification['message'] .= '</ul>';
    }
  }

  return $notification;
}


/**
 * Link URL Encode
 */
function doc_upload_format_url( $filename = '', $client_folder = '', $base_URL = 'https://hollandmortgageadvisors.sharepoint.com/WebUploads/Forms/AllItems.aspx?id=%2FWebUploads%2F' ) {
  $new_url = '';

  $file_code  = rawurlencode( $client_folder );
  $file_code .= '%2F';
  $file_code .= rawurlencode( $filename );
  $file_code .= '&parent=%2FWebUploads%2F';
  $file_code .= rawurlencode( $client_folder );

  $file_code = str_replace( '.', '%2E', $file_code );
  $file_code = str_replace( '-', '%2D', $file_code );

  $new_url = $base_URL . $file_code;

  return $new_url;
}
