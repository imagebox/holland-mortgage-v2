<?php
/**
 * Custom Shortcodes
 */

add_action( 'init', 'hma_custom_shortcodes' );
function hma_custom_shortcodes() {

  add_shortcode( 'apply_now_form', 'shortcode_apply_now_form' );
  function shortcode_apply_now_form( $atts ) {
    ob_start();
    get_template_part( 'custom-template-parts/apply-now-form' );
    return ob_get_clean();
  }
}
