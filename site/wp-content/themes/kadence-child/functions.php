<?php
/**
 * Kadence Child Functions
 * ===
 *
 * @package kadence-child
 */

function kadence_child_enqueue_styles() {
  
  /**
   * CSS
   */
  $tiny_slider_css_path  = get_stylesheet_directory_uri() . '/child-assets/css/vendor/tiny-slider.min.css';
  $tiny_slider_css_ver   = filemtime( get_stylesheet_directory() . '/child-assets/css/vendor/tiny-slider.min.css' );
  wp_enqueue_style( 'tiny-slider-style', $tiny_slider_css_path, array(), $tiny_slider_css_ver, 'screen' );

  $style_screen_path  = get_stylesheet_directory_uri() . '/style.css';
  $style_screen_ver   = filemtime( get_stylesheet_directory() . '/style.css' );
  wp_enqueue_style( 'kadence-child-css', $style_screen_path, array( 'tiny-slider-style' ), $style_screen_ver, 'screen' );

  /**
   * JS
   */
  $tiny_slider_path = get_stylesheet_directory_uri() . '/child-assets/js/libs/tiny-slider.min.js';
  $tiny_slider_ver  = filemtime( get_stylesheet_directory() . '/child-assets/js/libs/tiny-slider.min.js' );
  wp_enqueue_script( 'tiny-slider', $tiny_slider_path, array( 'jquery' ), $tiny_slider_ver, true );

  $script_site_path = get_stylesheet_directory_uri() . '/child-assets/js/kadence-child.js';
  $script_site_ver  = filemtime( get_stylesheet_directory() . '/child-assets/js/kadence-child.js' );
  wp_enqueue_script( 'kadence-child', $script_site_path, array( 'jquery', 'tiny-slider' ), $script_site_ver, true );


}
add_action('wp_enqueue_scripts', 'kadence_child_enqueue_styles');

if( function_exists('acf_add_options_page') ) {

  /**
   * Main Options Page
   */

  acf_add_options_page(array(
    'page_title'  => 'Options',
    'menu_title'  => 'Options',
    'menu_slug'   => 'options-page',
    'capability'  => 'edit_posts',
  ));


  /**
   * Sub-Pages
   */

  acf_add_options_sub_page(array(
    'page_title'  => 'Apply Now',
    'menu_title'  => 'Apply Now',
    'parent_slug' => 'options-page',
  ));
}


/**
 * Helper Functions
 */

/**
 * Convert US State name to abbreviation, and visa vera
 *
 * From Gist
 * [https://gist.github.com/maxrice/2776900?permalink_comment_id=2957386#gistcomment-2957386]
 * 
 * @param  string $name US State name or abbreviation
 * @param  bool   $req  Force the returend value to be an abbr or full name
 * @return string       Abbr or full name
 */
function state_abbr( $state ) {
  // from https://gist.github.com/maxrice/2776900 and http://www.comeexplorecanada.com/abbreviations.php
  $states = array( 'ALABAMA'=>'AL','ALASKA'=>'AK','ARIZONA'=>'AZ','ARKANSAS'=>'AR','CALIFORNIA'=>'CA','COLORADO'=>'CO','CONNECTICUT'=>'CT','DELAWARE'=>'DE','FLORIDA'=>'FL','GEORGIA'=>'GA','HAWAII'=>'HI','IDAHO'=>'ID','ILLINOIS'=>'IL','INDIANA'=>'IN','IOWA'=>'IA','KANSAS'=>'KS','KENTUCKY'=>'KY','LOUISIANA'=>'LA','MAINE'=>'ME','MARYLAND'=>'MD','MASSACHUSETTS'=>'MA','MICHIGAN'=>'MI','MINNESOTA'=>'MN','MISSISSIPPI'=>'MS','MISSOURI'=>'MO','MONTANA'=>'MT','NEBRASKA'=>'NE','NEVADA'=>'NV','NEW HAMPSHIRE'=>'NH','NEW JERSEY'=>'NJ','NEW MEXICO'=>'NM','NEW YORK'=>'NY','NORTH CAROLINA'=>'NC','NORTH DAKOTA'=>'ND','OHIO'=>'OH','OKLAHOMA'=>'OK','OREGON'=>'OR','PENNSYLVANIA'=>'PA','RHODE ISLAND'=>'RI','SOUTH CAROLINA'=>'SC','SOUTH DAKOTA'=>'SD','TENNESSEE'=>'TN','TEXAS'=>'TX','UTAH'=>'UT','VERMONT'=>'VT','VIRGINIA'=>'VA','WASHINGTON'=>'WA','WEST VIRGINIA'=>'WV','WISCONSIN'=>'WI','WYOMING'=>'WY','ALBERTA'=>'AB','BRITISH COLUMBIA'=>'BC','MANITOBA'=>'MB','NEW BRUNSWICK'=>'NB','NEWFOUNDLAND AND LABRADOR'=>'NL','NOVA SCOTIA'=>'NS','NORTWEST TERRITORIES'=>'NT','NUNAVUT'=>'NU','ONTARIO'=>'ON','PRINCE EDWARD ISLAND'=>'PE','QUEBEC'=>'QC','SASKATCHEWAN'=>'SK','YUKON'=>'YT','PUERTO RICO'=>'PR','VIRGIN ISLANDS'=>'VI','WASHINGTON DC'=>'DC' );

  // check for the full state name in the array
  if ( array_key_exists( strtoupper( $state ), $states ) ) {
    return $states[strtoupper( $state )];
  } else {
    return null;
  }
}


/**
 * CPTs
 */

require get_stylesheet_directory() . '/inc/cpts/cpt-team.php';


/**
 * Custom Includes
 */

require get_stylesheet_directory() . '/inc/custom-document-uploads.php';


/**
 * Shortcodes
 */

require get_stylesheet_directory() . '/inc/custom-shortcodes.php';
